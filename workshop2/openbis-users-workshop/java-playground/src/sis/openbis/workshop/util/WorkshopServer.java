package sis.openbis.workshop.util;

import ch.ethz.sis.openbis.generic.asapi.v3.IApplicationServerApi;
import ch.ethz.sis.openbis.generic.dssapi.v3.IDataStoreServerApi;
import ch.systemsx.cisd.common.spring.HttpInvokerUtils;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class WorkshopServer {
    public static final String AS_URL = "https://openbis-tst.ethz.ch" + "/openbis/openbis" + IApplicationServerApi.SERVICE_URL;
    public static final String DSS_URL = "https://openbis-tst.ethz.ch" + "/datastore_server" + IDataStoreServerApi.SERVICE_URL;
    private static final int TIMEOUT = Integer.MAX_VALUE;

    static {
        TrustManager[] trustAllCerts = new TrustManager[]
                { new X509TrustManager()
                {
                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers()
                    {
                        return null;
                    }

                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] certs,
                                                   String authType)
                    {
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] certs,
                                                   String authType)
                    {
                    }
                } };

        // Install the all-trusting trust manager
        try
        {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e)
        {
        }
    }

    public static IApplicationServerApi getASAPI() {
        return HttpInvokerUtils.createServiceStub(IApplicationServerApi.class, AS_URL, TIMEOUT);
    }

    public static IDataStoreServerApi getDSSAPI() {
        return HttpInvokerUtils.createServiceStub(IDataStoreServerApi.class, DSS_URL, TIMEOUT);
    }
}
