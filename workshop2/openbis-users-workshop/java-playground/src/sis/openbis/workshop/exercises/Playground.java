package sis.openbis.workshop.exercises;

import ch.ethz.sis.openbis.generic.asapi.v3.IApplicationServerApi;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.dataset.create.DataSetTypeCreation;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.entitytype.id.EntityTypePermId;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.experiment.create.ExperimentCreation;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.experiment.create.ExperimentTypeCreation;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.experiment.id.ExperimentPermId;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.project.Project;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.project.create.ProjectCreation;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.project.fetchoptions.ProjectFetchOptions;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.project.id.IProjectId;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.project.id.ProjectIdentifier;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.project.id.ProjectPermId;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.property.DataType;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.property.create.PropertyAssignmentCreation;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.property.create.PropertyTypeCreation;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.property.id.PropertyTypePermId;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.create.SampleTypeCreation;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.space.id.SpacePermId;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static sis.openbis.workshop.util.WorkshopServer.getASAPI;

public class Playground {

    private static final String SPACE_PERM_ID = "SPACE_CODE_CHANGEME";
    private static final String USER = "USER_CHANGEME";
    private static final String PASS = "PASS_CHANGEME";

    @Test
    public void login() {
        IApplicationServerApi v3 = getASAPI();
        String sessionToken = v3.login(USER, PASS);
        assertNotNull(sessionToken);
        v3.logout(sessionToken);
    }

    private String getCode(String postFix) {
        return USER + "." + postFix;
    }

    @Test
    public void createProperty() {
        PropertyTypeCreation creation = new PropertyTypeCreation();
        creation.setCode(getCode("PROPERTY_TYPE"));
        creation.setDataType(DataType.VARCHAR);
        creation.setLabel("Property Type Label");
        creation.setDescription("Property Type Description");

        String sessionToken = getASAPI().login(USER, PASS);
        List<PropertyTypePermId>  permIds = getASAPI().createPropertyTypes(sessionToken, Arrays.asList(creation));
        assertEquals(1, permIds.size());
    }

    @Test
    public void createExperimentType() {
        ExperimentTypeCreation creation = new ExperimentTypeCreation();
        creation.setCode(getCode("EXPERIMENT_TYPE"));
        PropertyAssignmentCreation ptCreation = new PropertyAssignmentCreation();
        ptCreation.setPropertyTypeId(new PropertyTypePermId(getCode("PROPERTY_TYPE")));
        creation.setPropertyAssignments(Arrays.asList(ptCreation));

        String sessionToken = getASAPI().login(USER, PASS);
        List<EntityTypePermId>  permIds = getASAPI().createExperimentTypes(sessionToken, Arrays.asList(creation));
        assertEquals(1, permIds.size());
    }

    @Test
    public void createSampleType() {
        SampleTypeCreation creation = new SampleTypeCreation();
        creation.setCode(getCode("SAMPLE_TYPE"));
        PropertyAssignmentCreation ptCreation = new PropertyAssignmentCreation();
        ptCreation.setPropertyTypeId(new PropertyTypePermId(getCode("PROPERTY_TYPE")));
        creation.setPropertyAssignments(Arrays.asList(ptCreation));

        String sessionToken = getASAPI().login(USER, PASS);
        List<EntityTypePermId>  permIds = getASAPI().createSampleTypes(sessionToken, Arrays.asList(creation));
        assertEquals(1, permIds.size());
    }

    @Test
    public void createDataSetType() {
        DataSetTypeCreation creation = new DataSetTypeCreation();
        creation.setCode(getCode("DATASET_TYPE"));
        PropertyAssignmentCreation ptCreation = new PropertyAssignmentCreation();
        ptCreation.setPropertyTypeId(new PropertyTypePermId(getCode("PROPERTY_TYPE")));
        creation.setPropertyAssignments(Arrays.asList(ptCreation));

        String sessionToken = getASAPI().login(USER, PASS);
        List<EntityTypePermId>  permIds = getASAPI().createDataSetTypes(sessionToken, Arrays.asList(creation));
        assertEquals(1, permIds.size());
    }

    @Test
    public void createProject() {
        ProjectCreation creation = new ProjectCreation();
        creation.setCode(getCode("PROJECT"));
        creation.setDescription("Description " + UUID.randomUUID().toString());
        creation.setSpaceId(new SpacePermId(SPACE_PERM_ID));

        String sessionToken = getASAPI().login(USER, PASS);
        List<ProjectPermId>  permIds = getASAPI().createProjects(sessionToken, Arrays.asList(creation));
        assertEquals(1, permIds.size());
    }

    @Test
    public void createExperiment() {
        ExperimentCreation creation = new ExperimentCreation();
        creation.setCode(getCode("EXPERIMENT"));
        creation.setProperty(getCode("PROPERTY_TYPE"), UUID.randomUUID().toString());
        ProjectIdentifier identifier = new ProjectIdentifier("/" + SPACE_PERM_ID + "/" + getCode("PROJECT"));
        creation.setProjectId(identifier);
        creation.setTypeId(new EntityTypePermId(getCode("EXPERIMENT_TYPE")));

        String sessionToken = getASAPI().login(USER, PASS);
        Map<IProjectId, Project> projectIdProjectMap = getASAPI().getProjects(sessionToken, Arrays.asList(identifier), new ProjectFetchOptions());
        if (projectIdProjectMap.isEmpty()) {
            createProject();
        }
        List<ExperimentPermId>  permIds = getASAPI().createExperiments(sessionToken, Arrays.asList(creation));
        assertEquals(1, permIds.size());
    }
}
