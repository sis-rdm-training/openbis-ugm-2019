REFERENCE

- openBIS Clients and APIS download link
https://wiki-bsse.ethz.ch/pages/viewpage.action?pageId=96536755
- openBIS V3 API link
https://wiki-bsse.ethz.ch/display/openBISDoc/openBIS+V3+API
- openBIS server for the workshop
https://openbis-tst.ethz.ch/openbis/
https://jupyterhub-tst.ethz.ch/