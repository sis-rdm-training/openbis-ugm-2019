How to create a dropbox
* create folder `<installation folder>/servers/core-plugins/my-plugin/1/dss/drop-boxes/simple-dropbox`
* add the example `plugin.properties` to this folder
* add the script `script.py` to this folder
* add to property `enabled-modules` of `<installation folder>/servers/core-plugins/core-plugins.properties` the new plugin `my-plugin`

How to run the dropbox
* stop DSS (by running `<installation folder>/bin/dssdown.sh`)
* start DSS (by running `<installation folder>/bin/dssup.sh`)
* drop a file/folder into `<installation folder>/data/incoming-my-simple-dropbox`

