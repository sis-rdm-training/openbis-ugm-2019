def process(transaction):
  # Create a data set of a particular type
  dataSet = transaction.createNewDataSet("RAW_DATA")
  
  # Reference the incoming file that was placed in the dropbox
  incoming = transaction.getIncoming()
  # Add the incoming file into the data set
  transaction.moveFile(incoming.getAbsolutePath(), dataSet)
  
  # Get an experiment for the data set
  exp = transaction.getExperiment("/DEFAULT/DEFAULT/DEFAULT")
 
  # Set the owner of the data set -- the specified experiment
  dataSet.setExperiment(exp)

  # Set a data set property
  dataSet.setPropertyValue("NAME", incoming.getName())