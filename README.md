# openBIS UGM 2019

Material for workshops at the openBIS User Group Meeting 2019.

In **workshop 2** participants will need the following VM:

https://polybox.ethz.ch/index.php/s/cX7DTCdm9DoJmuT

The VM can be opened with the VirtualBox program (https://www.virtualbox.org/)

VM username/password = openbis/openbis

openBIS username/password= admin/openbis


## pybis workshop

* install Jupyter [https://jupyter.org/install.html](https://jupyter.org/install.html)
* sample notebooks from the workshop: [https://sissource.ethz.ch/sispub/pybis-notebooks](https://sissource.ethz.ch/sispub/pybis-notebooks)
* more pybis features on [pypi.org](https://pypi.org/project/PyBIS/) (see documentation)
* [Jupyter Openbis Extension](https://pypi.org/project/jupyter-openbis-extension/) (a.k.a. the three openBIS-buttons in Jupyter)